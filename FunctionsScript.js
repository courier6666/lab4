import {randomUserMock, additionalUsers} from "./FE4U-Lab3-mock.js";
import countriesData from './CountriesToContinents.json' assert {type: 'json'};

var courses = ["Mathematics", "Physics", "English", "Computer Science", "Dancing", 
"Chess", "Biology", "Chemistry", "Law", "Art", "Medicine", "Statistics"];
export var usersData = [];

export var countriesToContinents = {};
var rawData = countriesData;


function FormatCountriesAndContinents()
{
    for(let i = 0;i<rawData.length;++i)
    {
        if(countriesToContinents[rawData[i].region]==undefined)
        countriesToContinents[rawData[i].region] = [];
        countriesToContinents[rawData[i].region].push(rawData[i].country);
    }
}
FormatCountriesAndContinents();

function randomIntegerGenerator(min, max)
{
    return Math.floor(Math.random() * (max - min)) + min;
}

export function ValidateUserData(user)
{
  
    if(typeof(user.full_name) != "string" || user.full_name == null || user.full_name[0].toUpperCase() !=  user.full_name[0])
        return false;

    if(typeof(user.gender) != "undefined")
    {
        if(typeof(user.gender) != "string" || user.gender == null || user.gender[0].toUpperCase() !=  user.gender[0])
            return false;
    }

    if(user.note != null && user.note.trim() != "" && typeof(user.note) != "undefined")
    {
        if(typeof(user.note) != "string" || user.note[0].toUpperCase() !=  user.note[0])
            return false;
    }
    
    if(typeof(user.state) != "undefined")
    {
        if(typeof(user.state) != "string" || user.state == null || user.state[0].toUpperCase() !=  user.state[0])
            return false;
    }
    
    if(typeof(user.city) != "undefined")
    {
        if(typeof(user.city) != "string" || user.city == null || user.city[0].toUpperCase() !=  user.city[0])
            return false;
    }

    if(typeof(user.country) != "undefined")
    {
        if(typeof(user.country) != "string" || user.country == null || user.country[0].toUpperCase() !=  user.country[0])
            return false;
    }
    
    if(typeof(user.age) != "undefined")
    {
        if(typeof(user.age) != "number")
            return false;
    }
    if(typeof(user.phone) != "undefined")
    {
        let phoneNumberFormat = /^[+]?[\s./0-9]*[(]?[0-9]{1,4}[)]?[-\s./0-9]*$/g;
        if(typeof(user.phone) != "string" || !phoneNumberFormat.test(user.phone))
        {
            return false;
        }
    }
    if(typeof(user.email) != "undefined")
    {
        let emailFormat = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
        if(typeof(user.email) != "string" || !emailFormat.test(user.email))
           return false;
    }
    return true;
}

function FormatUsersData()
{
    let users = [];
    for(let i = 0, userID = 0;i<randomUserMock.length;++i)
    {
        let newFormatUser = {};
        //tranforming randomUsersMockData to correct format
        let genderStr = randomUserMock[i].gender;

        newFormatUser.gender = genderStr[0].toUpperCase() + genderStr.substring(1,genderStr.length);
        newFormatUser.title = randomUserMock[i].name.title;
        newFormatUser.full_name = (randomUserMock[i].name.first + " " +randomUserMock[i].name.last).toString();
        newFormatUser.city = randomUserMock[i].location.city;
        newFormatUser.state = randomUserMock[i].location.state;
        newFormatUser.country = randomUserMock[i].location.country;
        newFormatUser.postcode = randomUserMock[i].location.postcode;
        newFormatUser.coordinates = randomUserMock[i].location.coordinates;
        newFormatUser.timezone = randomUserMock[i].location.timezone;
        newFormatUser.email = randomUserMock[i].email;
        newFormatUser.b_date = randomUserMock[i].dob.date;
        newFormatUser.age = randomUserMock[i].dob.age;
        newFormatUser.phone = randomUserMock[i].phone;
        newFormatUser.picture_large = randomUserMock[i].picture.large;
        newFormatUser.picture_thumbnail = randomUserMock[i].picture.thumbnail;
        
        newFormatUser.id = userID++;
        newFormatUser.favourite = (randomIntegerGenerator(0,10)%3 == 0);
        newFormatUser.course = courses[randomIntegerGenerator(0,courses.length)];
        newFormatUser.bg_color = "#C2CED3";
        newFormatUser.note = "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam aut veniam eveniet itaque, et debitis.";

        if(ValidateUserData(newFormatUser))users.push(newFormatUser);
    }
    for(let i = 0;i<additionalUsers.length;++i)
    {
        if(SearchUserBy(users, {full_name: additionalUsers[i].full_name})!=null)
            continue;
        let newFormatUser = {};
        newFormatUser.gender = additionalUsers[i].gender[0].toUpperCase() + additionalUsers[i].gender.substring(1, additionalUsers[i].gender.length);

        newFormatUser.full_name = additionalUsers[i].full_name;
        newFormatUser.city = additionalUsers[i].city;
        newFormatUser.state = additionalUsers[i].state;
        newFormatUser.country = additionalUsers[i].country;
        newFormatUser.postcode = additionalUsers[i].postcode;
        newFormatUser.coordinates = additionalUsers[i].coordinates;
        newFormatUser.timezone = additionalUsers[i].timezone;
        newFormatUser.email = additionalUsers[i].email;
        newFormatUser.b_date = additionalUsers[i].b_day;
        newFormatUser.age = additionalUsers[i].age;
        newFormatUser.phone = additionalUsers[i].phone;
        newFormatUser.picture_large = additionalUsers[i].picture_large;
        newFormatUser.picture_thumbnail = randomUserMock[i].picture_thumbnail;
        
        newFormatUser.id = additionalUsers[i].id;
        newFormatUser.favourite = additionalUsers[i].favorite;
        newFormatUser.course = courses[randomIntegerGenerator(0,courses.length)];
        newFormatUser.bg_color = additionalUsers[i].bg_color;
        if(newFormatUser.note != null)newFormatUser.note = additionalUsers[i].note[0].toUpperCase() + additionalUsers[i].note.substring(1,additionalUsers[i].note.length);
        if(ValidateUserData(newFormatUser))users.push(newFormatUser);
    }
    return users;
}

export function FilterUsers(dataInput,parameters)
{
    let filterResult = [];
    
    for(let i = 0;i<dataInput.length;++i)
    {
        let currentComparison = true;
        for(let j = 0;j<parameters.length;++j)
        {
            let currentParameterValueComparison = true;
            if(typeof(parameters[j].range) != "undefined")
            {
                currentParameterValueComparison &= parameters[j].sat_cond ?
                (parameters[j].range[0]<=dataInput[i][parameters[j].key] &&
                dataInput[i][parameters[j].key]<=parameters[j].range[1]) :
                !(parameters[j].range[0]<=dataInput[i][parameters[j].key] &&
                dataInput[i][parameters[j].key]<=parameters[j].range[1]);
            }
            else
            {
                if(!parameters.sat_all_cond)currentParameterValueComparison = false;
                for(let k = 0;k<parameters[j].values.length;++k)
                {
                    if(parameters.sat_all_cond)
                    {
                        currentParameterValueComparison &= parameters[j].sat_cond ?
                        (dataInput[i][parameters[j].key] == parameters[j].values[k]) :
                        !(dataInput[i][parameters[j].key] == parameters[j].values[k]);
                    }
                    else 
                    {
                        currentParameterValueComparison |= parameters[j].sat_cond ?
                        (dataInput[i][parameters[j].key] == parameters[j].values[k]) :
                        !(dataInput[i][parameters[j].key] == parameters[j].values[k]);
                    }
                }
            }
            currentComparison &= currentParameterValueComparison;
        }
        if(currentComparison)filterResult.push(dataInput[i]);
    }
    
    return filterResult;
}

export function FilterUsersPercentage(dataInput, parameters)
{
    let filterResult = FilterUsers(dataInput, parameters);
    return (filterResult.length / dataInput.length) * 100;
}

export function SearchUserBy(dataInput, parameters)
{
    let parameterEntries = Object.entries(parameters);
    for(let i = 0;i<dataInput.length;++i)
    {
        for(let j = 0;j<parameterEntries.length;++j)
        {
            if(typeof(dataInput[i][parameterEntries[j][0]]) == "string")
            {
                if(dataInput[i][parameterEntries[j][0]].includes(parameterEntries[j][1]))return dataInput[i];
            }
            else if(dataInput[i][parameterEntries[j][0]] == parameterEntries[j][1])return dataInput[i];
        }
    }
    return null;
}
function CompareElements(a, b)
{
    let dataType = typeof(a);

    switch(dataType)
    {
        case "string":
            return a.localeCompare(b);
        case "number":
            return a - b;
        case "date":
            return a - b;
    }
}
export function SortUsers(inputData, keyParameters, isAsce = true)
{
    let multiplierSortOrder = 1;
    if(!isAsce)multiplierSortOrder = -1;
    inputData.sort((a, b) => {
        for(let i = 0;i < keyParameters.length;++i)
        {
            let compareResult = CompareElements(a[keyParameters[i]], b[keyParameters[i]]);
            if(compareResult != 0)return compareResult * multiplierSortOrder;
        }
        return 0;
    });
}

usersData = FormatUsersData();