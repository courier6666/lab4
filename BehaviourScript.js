import { usersData } from "./FunctionsScript.js";
import { FilterUsers } from "./FunctionsScript.js";
import { SortUsers } from "./FunctionsScript.js";
import { FilterUsersPercentage } from "./FunctionsScript.js";
import { SearchUserBy } from "./FunctionsScript.js";
import { ValidateUserData } from "./FunctionsScript.js";
import { countriesToContinents } from "./FunctionsScript.js";


function RemoveAllChildren(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}



var mapIsOpen = false;
const ToggleMap = document.getElementById("toggleMapLink");
ToggleMap.addEventListener("click", function(e)
    {
        mapIsOpen = ~mapIsOpen;
        TeacherInfoMap.style.display = mapIsOpen ? "block" :"none";
        
    });

const TeacherInfoPicture = document.getElementById("teacherInfoPicture");
const TeacherInfoName = document.getElementById("fullNameTeacherInfo");
const TeacherInfoCity = document.getElementById("cityTeacherInfo");
const TeacherInfoCountry = document.getElementById("countryTeacherInfo");
const TeacherInfoAge = document.getElementById("ageTeacherInfo");
const TeacherInfoSex = document.getElementById("sexTeacherInfo");
const TeacherInfoEmail = document.getElementById("emailTeacherInfo");
const TeacherInfoPhone = document.getElementById("phoneNumberTeacherInfo");
const TeacherInfoNote = document.getElementById("commentTeacherInfo");
const TeacherInfoMap = document.getElementById("mapImageInfo");
const TeacherInfoFavouriteButton = document.getElementById("setFavouriteButton");
const fullCardContent = document.getElementById("fullCardContent");

var currentSelectedTeacher = null;
var currentTeachers = usersData;

function DisplayCurrentTeachers()
{
    RemoveAllChildren(document.getElementById("topTeachersDisplayPanel"));
    for(let i = 0;i<currentTeachers.length;++i)
    {
        DisplayCompactCardAtPanel(currentTeachers[i], "topTeachersDisplayPanel");
    }
}

//Teacher form. Add new teacher
var teacherForm = document.getElementById("teacherForm");
teacherForm.addEventListener("submit", function(event)
{
    event.preventDefault();
    let fullName = document.getElementById("fullNameInput");
    let specialty = document.getElementById("specialtyInput");
    let email = document.getElementById("emailInput");
    let phoneNumber = document.getElementById("phoneNumberInput");
    let country = document.getElementById("countryInput");
    let city = document.getElementById("cityInput");
    let birthdayDate = document.getElementById("BirthdayDateInput");
    let genderMale = document.getElementById("RadioButtonMaleInput");
    let backgroundColor = document.getElementById("backgroudColorInput");
    let comment = document.getElementById("noteInputField");

    let newUser = {};
    newUser.full_name = fullName.value;
    newUser.course = specialty.value;
    newUser.email = email.value;
    newUser.phone = phoneNumber.value;
    newUser.country = country.value;
    newUser.city = city.value;
    newUser.b_date = birthdayDate.value;
    newUser.coordinates = {};
    let dateValues = birthdayDate.value.split('-');
    let currentDate = new Date();
    newUser.age = currentDate.getFullYear() - parseInt(dateValues[0]);
    if(currentDate.getMonth()<parseInt(dateValues[1]))
    {
        --newUser.age;
    }
    else if(currentDate.getMonth() == parseInt(dateValues[1]))
    {
        if(currentDate.getDay()<parseInt(dateValues[2]))--newUser.age;
    }
    newUser.gender = genderMale.checked ? "Male" : "Female";
    newUser.bg_color = backgroundColor.value;
    
    newUser.note = comment.value.trim();
    if(SearchUserBy(usersData, {full_name: newUser.full_name}) == null && ValidateUserData(newUser))
    {   

        let requestOptions = {
            method: 'GET',
          };
        let obj;
        fetch("https://api.geoapify.com/v1/geocode/search?text=11%20" + newUser.city + "%2C%20"+ newUser.country + "&apiKey=c4d7028bdaf64112a3320b300e0ff1e0")
        .then(res => res.json())
        .then(data => {
            newUser.coordinates.latitude = data.features[0].bbox[1];
            newUser.coordinates.longitude = data.features[0].bbox[0];
         });
        usersData.push(newUser);
        UpdateCurrentTeachers();
    }
});
//

document.getElementById("searchButton").addEventListener("click",function()
{
    let searchBoxValue = document.getElementById("searchTextBox").value;
    let overlay = document.getElementById("overlay");
    let popupInfo = document.getElementById("popupTeacherInfo");
    if(searchBoxValue != "")
    {
        let foundUserByFullname = SearchUserBy(usersData, {full_name: searchBoxValue});
        if(foundUserByFullname != null)
        {
            ChangePopupTeacherInfo(foundUserByFullname);
            document.getElementById("refToTeacherInfo").click();
            return;
        }
        let foundUserByNote = SearchUserBy(usersData, {note: searchBoxValue});
        if(foundUserByNote != null)
        {
            ChangePopupTeacherInfo(foundUserByNote);
            document.getElementById("refToTeacherInfo").click();
            return;
        }
        let foundUserByAge = SearchUserBy(usersData, {age: parseInt(searchBoxValue)});
        if(foundUserByAge != null)
        {
            ChangePopupTeacherInfo(foundUserByAge);
            document.getElementById("refToTeacherInfo").click();
            return;
        }
    }
});

function ChangePopupTeacherInfo(teacher)
{
    TeacherInfoPicture.src = teacher.picture_large;
    TeacherInfoFavouriteButton.innerHTML = teacher.favourite ? "&starf;" : "&star;";
    TeacherInfoFavouriteButton.className = teacher.favourite ? "setFavouriteButtonTrue" : "setFavouriteButtonFalse";
    TeacherInfoName.innerHTML = teacher.full_name;
    TeacherInfoCity.innerHTML = teacher.city;
    TeacherInfoCountry.innerHTML = teacher.country;
    TeacherInfoAge.innerHTML = teacher.age;
    TeacherInfoSex.innerHTML = teacher.gender;
    fullCardContent.style.backgroundColor = teacher.bg_color;

    const emailRef = document.createElement("a");
    emailRef.href = "";
    emailRef.className = "emailLink";
    emailRef.innerHTML = teacher.email;
    RemoveAllChildren(TeacherInfoEmail);
    TeacherInfoEmail.append(emailRef);

    TeacherInfoPhone.innerHTML = teacher.phone;
    
    TeacherInfoNote.innerHTML = teacher.note;

    TeacherInfoMap.src = "https://maps.google.com/maps?q=" + teacher.coordinates.latitude + "," + teacher.coordinates.longitude + "&hl=es;z=7&output=embed";
}

function СardClickHandler(elem, teacher)
{
    elem.addEventListener("click", function(e){
            currentSelectedTeacher = teacher;
            ChangePopupTeacherInfo(teacher);
        }
    );
}

TeacherInfoFavouriteButton.addEventListener("click", function()
{
    if(TeacherInfoFavouriteButton.className == "setFavouriteButtonTrue")
    {
        TeacherInfoFavouriteButton.className = "setFavouriteButtonFalse";
        TeacherInfoFavouriteButton.innerHTML = "&star;";
        currentSelectedTeacher.favourite = false;
    }
    else
    {
        TeacherInfoFavouriteButton.className = "setFavouriteButtonTrue";
        TeacherInfoFavouriteButton.innerHTML = "&starf;";
        currentSelectedTeacher.favourite = true;
    }
    RemoveAllChildren(document.getElementById("topTeachersDisplayPanel"));
    for(let i = 0;i<currentTeachers.length;++i)
    {
        DisplayCompactCardAtPanel(currentTeachers[i], "topTeachersDisplayPanel");  
    }
    favUsers = FavouriteTeachers();
    maxFavUsersTabs = Math.ceil(favUsers.length / currentTeachersPerTabLimit);
    DisplayFavouriteTeachers();
});

function DisplayCompactCardAtPanel(teacher, displayPanelID)
{
    const displayTeachers = document.getElementById(displayPanelID);

    const teacherCompactCard = document.createElement("div");
    teacherCompactCard.className = "teacherCompactCard";
    teacherCompactCard.id = "Teacher" + teacher.id;

    const pictureWrap = document.createElement("div");
    pictureWrap.className  = "pictureDivWrap";
    СardClickHandler(pictureWrap, teacher);
    
    const favourite = document.createElement("div");
    favourite.className = "isFavourite";
    favourite.innerHTML = "&starf;";
    favourite.style.visibility = teacher.favourite ? "visible" : "hidden";
    
    const anchorInfoPopup = document.createElement("a");
    anchorInfoPopup.href = "#popupTeacherInfo";
    
    const pictureDiv = document.createElement("div");
    pictureDiv.className = "pictureDiv";
    
    const initials = document.createElement("label");
    initials.className = "alternativeInitials";
    for(let i = 0;i<teacher.full_name.length;++i)
    {
        if(teacher.full_name[i].toUpperCase() ==  teacher.full_name[i] && teacher.full_name[i]!=" ")
            initials.innerHTML += teacher.full_name[i] + ".";
    }
    
    const teacherPicturePreview = document.createElement("div");
    teacherPicturePreview.className = "teacherPicturePreview";
    teacherPicturePreview.style.backgroundImage = "url('" + teacher.picture_large + "')";
    
    pictureDiv.append(initials);
    pictureDiv.append(teacherPicturePreview);
    
    anchorInfoPopup.append(pictureDiv);
    
    pictureWrap.append(favourite);
    pictureWrap.append(anchorInfoPopup);
    
    teacherCompactCard.append(pictureWrap);
    
    const fullNameDiv = document.createElement("div");
    fullNameDiv.id = "fullNameDiv";
    
    const fullNameLabel = document.createElement("label");
    fullNameLabel.id = "fullName";
    fullNameLabel.className = "teacherName";
    
    let names = teacher.full_name.split(' ');

    const spanFirstName = document.createElement("span");
    spanFirstName.innerHTML = names[0];
    
    const spanLastName = document.createElement("span");
    spanLastName.innerHTML = names[1];
    
    fullNameLabel.append(spanFirstName);
    fullNameLabel.append(spanLastName);;
    
    fullNameDiv.append(fullNameLabel);
    
    teacherCompactCard.append(fullNameDiv);
    
    const countryDiv = document.createElement("div");
    countryDiv.id = "countryOriginDiv";
    
    const countryLabel = document.createElement("label");
    countryLabel.id = "country";
    countryLabel.innerHTML = teacher.country;
    
    countryDiv.append(countryLabel);
    
    teacherCompactCard.append(countryDiv);
    displayTeachers.append(teacherCompactCard);
    
}

for(let i = 0;i<usersData.length;++i)
{
    DisplayCompactCardAtPanel(usersData[i], "topTeachersDisplayPanel");
}

//Filter parameters
const ageParam = document.getElementById("ageComboBox");
const regionParam = document.getElementById("regionComboBox");
const sexParam = document.getElementById("sexComboBox");
const withPhotoParam = document.getElementById("onlyPhotoCheckbox");
const favouritesParam = document.getElementById("onlyFavouritesCheckbox");

ageParam.addEventListener("change", UpdateCurrentTeachers);
regionParam.addEventListener("change", UpdateCurrentTeachers);
sexParam.addEventListener("change", UpdateCurrentTeachers);
withPhotoParam.addEventListener("change", UpdateCurrentTeachers);
favouritesParam.addEventListener("change", UpdateCurrentTeachers);

function UpdateCurrentTeachers()
{
    let parameters = [];
    if(ageParam.value != "")
    {
        let ages = ageParam.value.split('-');
        let agesParsed = [parseInt(ages[0]), parseInt(ages[1])];
        parameters.push({key: "age", range: agesParsed, sat_cond: true, sat_all_cond: true});
    }
    if(regionParam.value != "")
    {
        parameters.push({key: "country", values: countriesToContinents[regionParam.value], sat_cond: true, sat_all_cond: false});
    }
    if(sexParam.value != "")
    {
        let sex = sexParam.value;
        parameters.push({key: "gender", values: [sex], sat_cond: true, sat_all_cond: true});
    }
    if(withPhotoParam.checked)
    {
        parameters.push({key: "picture_large", values: [undefined, null], sat_cond: false, sat_all_cond: true});
    }
    if(favouritesParam.checked)
    {
        parameters.push({key: "favourite", values: [true], sat_cond: true, sat_all_cond: true});
    }
    currentTeachers = FilterUsers(usersData, parameters);
    DisplayCurrentTeachers();
}



//Stats table
const tableStats = document.getElementById("tableStatsTeachers");
const FullnameHeading = document.getElementById("fullnameHeading");
const CourseHeading = document.getElementById("SpecialtyHeading");
const AgeHeading = document.getElementById("AgeHeading");
const GenderHeading = document.getElementById("GenderHeading");
const CountryHeading = document.getElementById("CountryHeading");

function OnHeadingClickHandler(heading, parameter)
{
    heading.addEventListener("click", function()
    {
        let headingState;
        if(heading.className == "tableHeading")headingState = "notActive";
        else if(heading.className == "tableHeadingActiveAsceOrder")headingState = "AsceActive";
        else if(heading.className == "tableHeadingActiveDescOrder")headingState = "DescActive";
        FullnameHeading.className = "tableHeading";
        CourseHeading.className = "tableHeading";
        AgeHeading.className = "tableHeading";
        GenderHeading.className = "tableHeading";
        CountryHeading.className = "tableHeading";
        switch(headingState)
        {
            case "notActive":
                heading.className = "tableHeadingActiveAsceOrder";
                UpdateTableStatsBasedOnParameters([parameter], true);
                DisplayTable();
            break;
            case "AsceActive": 
                heading.className = "tableHeadingActiveDescOrder";
                UpdateTableStatsBasedOnParameters([parameter], false);
                DisplayTable();
            break;
            case "DescActive":
                heading.className = "tableHeading";
                UpdateTableStatsBasedOnParameters([]);
                DisplayTable();
            break;
        }
        console.log(heading.className);
    });
}

OnHeadingClickHandler(FullnameHeading, "full_name");
OnHeadingClickHandler(CourseHeading, "course");
OnHeadingClickHandler(AgeHeading, "age");
OnHeadingClickHandler(GenderHeading, "gender");
OnHeadingClickHandler(CountryHeading, "country");


var currentTableStatTab = 0;
const maxTeachersPerTab = 10;
var maxTableTabs = Math.ceil(usersData.length / maxTeachersPerTab);
var sortedUsers;

function UpdateTableStatsBasedOnParameters(parameters, isAsce = true)
{
    sortedUsers = [...usersData];
    SortUsers(sortedUsers, parameters, isAsce);
}
function DisplayTable()
{
    for(let i = 1;i<tableStats.rows.length;)
    tableStats.deleteRow(i);
    for(let i = currentTableStatTab * maxTeachersPerTab;i<Math.min(currentTableStatTab * maxTeachersPerTab + maxTeachersPerTab, sortedUsers.length);++i)
    {
        let row = document.createElement("tr");

        let nameCell = document.createElement("td");
        nameCell.innerHTML = sortedUsers[i].full_name;

        let specialtyCell = document.createElement("td");
        specialtyCell.innerHTML = sortedUsers[i].course;

        let ageCell = document.createElement("td");
        ageCell.innerHTML = sortedUsers[i].age;

        let genderCell = document.createElement("td");
        genderCell.innerHTML = sortedUsers[i].gender;

        let countryCell = document.createElement("td");
        countryCell.innerHTML = sortedUsers[i].country;

        row.append(nameCell);
        row.append(specialtyCell);
        row.append(ageCell);
        row.append(genderCell);
        row.append(countryCell);
        tableStats.append(row);
    }
}

UpdateTableStatsBasedOnParameters([]);
DisplayTable();

var navigationTableButtons = document.getElementById("tableNavigationButtons");

function OnNavigationButtonClick(NavButton)
{
    NavButton.addEventListener("click", function()
    {
        currentTableStatTab = parseInt(NavButton.innerHTML) - 1;
        DisplayTable();
        CreateNavigationButtons();
    });
}

function CreateNavigationButtons()
{
    RemoveAllChildren(navigationTableButtons);
    if(maxTableTabs <= 4)
    {
        for(let i = 1;i <= maxTableTabs;++i)
        {
            let NavButton = document.createElement("button");
            NavButton.innerHTML = i;
            NavButton.className = "tableNavigationButton";
            if((i - 1) == currentTableStatTab)NavButton.className = "tableNavigationButtonSelected";
            navigationTableButtons.append(NavButton);
            OnNavigationButtonClick(NavButton);
        }
        return;
    }

    let SelectedButton = document.createElement("button");
    SelectedButton.innerHTML = (currentTableStatTab + 1);
    SelectedButton.className = "tableNavigationButtonSelected";
    OnNavigationButtonClick(SelectedButton);
    if(currentTableStatTab < 2)
    {
        for(let i = 1;i <= 3;++i)
        {
            if(currentTableStatTab + 1 != i)
            {
                let NavButton = document.createElement("button");
                NavButton.innerHTML = i;
                NavButton.className = "tableNavigationButton";
                navigationTableButtons.append(NavButton);
                OnNavigationButtonClick(NavButton);
            }
            else 
            {
                navigationTableButtons.append(SelectedButton);
            }
        }

        let rangeButton = document.createElement("button");
        rangeButton.innerHTML = "...";
        rangeButton.className = "rangeNavigationButton";

        let lastButton = document.createElement("button");
        lastButton.innerHTML = maxTableTabs;
        lastButton.className = "tableNavigationButton";
        OnNavigationButtonClick(lastButton);

        navigationTableButtons.append(rangeButton);
        navigationTableButtons.append(lastButton);
        return;
    }
    let firstTabButton = document.createElement("button");
    firstTabButton.innerHTML = "1";
    firstTabButton.className = "tableNavigationButton";
    navigationTableButtons.append(firstTabButton);
    OnNavigationButtonClick(firstTabButton);

    if(currentTableStatTab > 2)
    {
        let rangeButton = document.createElement("button");
        rangeButton.innerHTML = "...";
        rangeButton.className = "rangeNavigationButton";
        navigationTableButtons.append(rangeButton);
    }
    for(let currentTab = currentTableStatTab;currentTab <= Math.min(currentTableStatTab + 2, maxTableTabs);++currentTab)
    {
        if(currentTab - 1 == currentTableStatTab)
        {
            navigationTableButtons.append(SelectedButton);
            continue;
        }
        let NavButton = document.createElement("button");
        NavButton.innerHTML = currentTab;
        NavButton.className = "tableNavigationButton";
        OnNavigationButtonClick(NavButton);
        navigationTableButtons.append(NavButton);
    }
    if(maxTableTabs - (currentTableStatTab + 1) > 2)
    {
        let rangeButton = document.createElement("button");
        rangeButton.innerHTML = "...";
        rangeButton.className = "rangeNavigationButton";
        navigationTableButtons.append(rangeButton);
    }
    if(maxTableTabs - (currentTableStatTab + 1) >= 2)
    {
        let lastButton = document.createElement("button");
        lastButton.innerHTML = maxTableTabs;
        lastButton.className = "tableNavigationButton";
        OnNavigationButtonClick(lastButton);
        navigationTableButtons.append(lastButton);
    }
}

CreateNavigationButtons();

//Favourite teachers
var favUsers = FilterUsers(usersData,[{key: "favourite", values: [true], sat_cond: true, sat_all_cond: true}]);
var currentFavouriteUsersTab = 0;
var currentTeachersPerTabLimit = 6;
var previousWidth = document.getElementById("favouriteTeachers").offsetWidth;
var maxFavUsersTabs = Math.ceil(favUsers.length / currentTeachersPerTabLimit);

document.getElementById("nextTeacher").addEventListener("click", function(){
    if(currentFavouriteUsersTab<maxFavUsersTabs - 1)
    {
        ++currentFavouriteUsersTab;
        DisplayFavouriteTeachers();
    }
});
document.getElementById("prevTeacher").addEventListener("click", function(){
    if(currentFavouriteUsersTab > 0)
    {
        --currentFavouriteUsersTab;
        DisplayFavouriteTeachers();
    }
});

function ChangeFavUsersLimit()
{
    let pageWidth = document.getElementById("favouriteTeachers").offsetWidth;
    if(previousWidth == pageWidth)return false;
    previousWidth = pageWidth;
    currentTeachersPerTabLimit = Math.floor(pageWidth / 120); 
    maxFavUsersTabs = Math.ceil(favUsers.length / currentTeachersPerTabLimit);
    return true;
}

function OnResizeFavTeachersDisplay()
{
    if(ChangeFavUsersLimit()) DisplayFavouriteTeachers();
}

window.onresize = OnResizeFavTeachersDisplay;

function FavouriteTeachers()
{
    return FilterUsers(usersData,[{key: "favourite", values: [true], sat_cond: true, sat_all_cond: true}]);
}

function DisplayFavouriteTeachers()
{
    RemoveAllChildren(document.getElementById("favouriteDisplayPanel"));
    for(let i = currentFavouriteUsersTab * currentTeachersPerTabLimit;i<Math.min(currentFavouriteUsersTab * currentTeachersPerTabLimit + currentTeachersPerTabLimit, favUsers.length);++i)
    {
        DisplayCompactCardAtPanel(favUsers[i], "favouriteDisplayPanel");
    }
}

DisplayFavouriteTeachers();
